package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/xanzy/go-gitlab"
)

var (
	key        = "automerge"
	reportMark = fmt.Sprintf("<!-- memo \"%s\" -->", key)
	jsonMark   = fmt.Sprintf("<!-- json \"%s\" -->", key)
)

// Memo is the record keeping structure used for assignment
type Memo struct {
	MergeRequestIID *int    `json:"merge_request_iid,omitempty"`
	CommitSHA       *string `json:"commit_sha,omitempty"`
}

// UnmarshalMemo unmarshals the merge request and commit from the input string
func UnmarshalMemo(client *gitlab.Client, projectID string, value string) (*gitlab.MergeRequest, *gitlab.Commit, error) {
	jsonParts := strings.Split(value, jsonMark)
	switch len(jsonParts) {
	case 1:
		return nil, nil, nil
	case 3:
	default:
		return nil, nil, fmt.Errorf("expected exactly two occurrences of `%s`, got %d", jsonMark, len(jsonParts))
	}

	memo := &Memo{}
	err := json.Unmarshal([]byte(trim(jsonParts[1], "<!-- ", " -->")), memo)
	if err != nil {
		return nil, nil, err
	}

	if memo.MergeRequestIID != nil {
		mr, err := GetMergeRequest(client, projectID, *memo.MergeRequestIID)
		if err != nil {
			return nil, nil, err
		}

		return mr, nil, nil
	}

	if memo.CommitSHA != nil {
		commit, err := GetCommit(client, projectID, *memo.CommitSHA)
		if err != nil {
			return nil, nil, err
		}

		return nil, commit, nil
	}

	return nil, nil, nil
}

// MarshalMemo marshalls the automerge struct to description
func (merge AutoMerge) MarshalMemo() (string, string, error) {
	var (
		title       string
		description string
		memo        Memo
	)

	if merge.triggerMergeRequest != nil {
		title = fmt.Sprintf("%s: %s", merge.targetBranch, merge.triggerMergeRequest.Title)
		description = fmt.Sprintf("Automatic merge of **!%d**", merge.triggerMergeRequest.IID)

		memo = Memo{
			MergeRequestIID: &merge.triggerMergeRequest.IID,
		}
	}

	if merge.triggerCommit != nil {
		title = fmt.Sprintf("%s: %s", merge.targetBranch, merge.triggerCommit.Title)
		description = fmt.Sprintf("Automatic merge of **%s**", merge.triggerCommit.ShortID)

		memo = Memo{
			CommitSHA: &merge.triggerCommit.ID,
		}
	}

	// Append JSON memo for machine readability
	val, err := json.Marshal(memo)
	if err != nil {
		return "", "", err
	}
	description += fmt.Sprintf("\n\n%s\n<!-- %s -->\n%s\n", jsonMark, string(val), jsonMark)

	return title, description, nil
}

// trim removes all surrounding whitespace, then both the prefix and suffix
func trim(s string, prefix string, suffix string) string {
	return strings.TrimPrefix(strings.TrimSuffix(strings.TrimSpace(s), suffix), prefix)
}
