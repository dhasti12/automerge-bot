package main

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"regexp"
	"testing"

	"github.com/xanzy/go-gitlab"
)

var (
	reBranches               = regexp.MustCompile(`api/v4/projects/.+/repository/branches`)
	reMergeRequestsForCommit = regexp.MustCompile(`api/v4/projects/.+/repository/commits/.+/merge_requests`)
	reCommits                = regexp.MustCompile(`api/v4/projects/.+/repository/commits`)
	reMergeRequests          = regexp.MustCompile(`api/v4/projects/.+/merge_requests`)
)

func gitlabMockHandler(w http.ResponseWriter, r *http.Request) {
	var (
		f   *os.File
		err error
	)

	switch {
	case reBranches.MatchString(r.URL.Path):
		f, err = os.Open("testdata/list_branches.json")
	case reMergeRequestsForCommit.MatchString(r.URL.Path):
		f, err = os.Open("testdata/list_merge_requests.json")
	case reCommits.MatchString(r.URL.Path):
		fname := fmt.Sprintf("testdata/commit_%s.json", path.Base(r.URL.Path))
		f, err = os.Open(fname)
	case reMergeRequests.MatchString(r.URL.Path):
		fname := fmt.Sprintf("testdata/mr_%s.json", path.Base(r.URL.Path))
		f, err = os.Open(fname)
	}

	if f == nil || err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}

	io.Copy(w, f)
}

// source commit:
// - 1 parent
// - 2 parents (without merge request)
// - 2 parents (with merge request, no memo)
// - 2 parents (with merge request with mr memo)
// - 2 parents (with merge request with commit memo)
//
// merge requests
// - 1 with memo with merge request id
// - 1 with memo with commit id
// - 1 without memo
func TestFindTrigger(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(gitlabMockHandler))
	defer ts.Close()

	client := gitlab.NewClient(nil, "")
	err := client.SetBaseURL(ts.URL)
	if err != nil {
		t.Fatal(err)
	}

	var (
		simpleCommit      = "2ad5b30b6c02a3e3f84275121a709f5de75dac48"
		simpleMergeCommit = "15916ad55920ca582a9124f7f0737b0373432a99"
		gitlabMergeCommit = "3e4862ac6d492807a0b2a6ba3dde819716dbfae1"
		autoMergeRequest  = "523ed2e72962e97be2603ab78aa13b372da582fa"
		autoMergeCommit   = "9209736a22501dd4ea16afee86d0968bead84100"
	)

	if triggerMergeRequest, triggerCommit, err := FindTrigger(client, "example-project", "release/10.4", simpleCommit); err != nil {
		t.Fatal(err)
	} else if triggerMergeRequest != nil {
		t.Fatalf("expected trigger merge request to be nil")
	} else if triggerCommit.ID != simpleCommit {
		t.Fatalf("expected trigger commit to be %s, got %s", simpleCommit, triggerCommit.ID)
	}

	if triggerMergeRequest, triggerCommit, err := FindTrigger(client, "example-project", "release/10.4", simpleMergeCommit); err != nil {
		t.Fatal(err)
	} else if triggerMergeRequest != nil {
		t.Fatalf("expected trigger merge request to be nil")
	} else if triggerCommit.ID != simpleMergeCommit {
		t.Fatalf("expected trigger commit to be %s, got %s", simpleMergeCommit, triggerCommit.ID)
	}

	if triggerMergeRequest, triggerCommit, err := FindTrigger(client, "example-project", "release/10.4", gitlabMergeCommit); err != nil {
		t.Fatal(err)
	} else if triggerCommit != nil {
		t.Fatalf("expected trigger commit to be nil")
	} else if triggerMergeRequest.IID != 123 {
		t.Fatalf("expected trigger merge request to be !%d, got %d", 123, triggerMergeRequest.IID)
	}

	if triggerMergeRequest, triggerCommit, err := FindTrigger(client, "example-project", "release/10.4", autoMergeRequest); err != nil {
		t.Fatal(err)
	} else if triggerCommit != nil {
		t.Fatalf("expected trigger commit to be nil")
	} else if triggerMergeRequest.IID != 99 {
		t.Fatalf("expected trigger merge request to be !%d, got %d", 99, triggerMergeRequest.IID)
	}

	if triggerMergeRequest, triggerCommit, err := FindTrigger(client, "example-project", "release/10.4", autoMergeCommit); err != nil {
		t.Fatal(err)
	} else if triggerMergeRequest != nil {
		fmt.Println(triggerMergeRequest)
		t.Fatalf("expected trigger merge request to be nil")
	} else if triggerCommit.ID != "14cc2c372c7d41b8fa05f851fc9111116e121a7a" {
		t.Fatalf("expected trigger commit to be !%s, got %s", "14cc2c372c7d41b8fa05f851fc9111116e121a7a", triggerCommit.ID)
	}
}

func TestPrepareAutoMerge(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(gitlabMockHandler))
	defer ts.Close()

	client := gitlab.NewClient(nil, "")
	err := client.SetBaseURL(ts.URL)
	if err != nil {
		t.Fatal(err)
	}

	opts := Options{
		projectID:      "example-project",
		sourceBranch:   "release/10.1",
		sourceSHA:      "523ed2e72962e97be2603ab78aa13b372da582fa",
		semverPrefix:   "release/",
		defaultBranch:  "develop",
		mergeToDefault: true,
	}
	if merge, err := prepareAutoMerge(client, opts); err != nil {
		t.Fatal(err)
	} else {
		expectedTempBranch := "automerge/99/release/10.2"
		if merge.tempBranch != expectedTempBranch {
			t.Fatalf("expected `%s`, got `%s`", expectedTempBranch, merge.tempBranch)
		}
	}

}
